﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepManager : MonoBehaviour {

    public GameObject[] stepStorys;
    int animeNum = 0;
    public bool legend = false;


    public enum StepName
    {
        InGame,
        Ikaku,
        Ending,
        Result,
    };
    StepName step = StepName.InGame;

    public bool checkStep(StepName checkStepName)
    {
        return (checkStepName == step);
    }
    
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public void setNextStory()
    {
        setStepStory(animeNum + 1);
    }

    public void setStepStory(int setStep)
    {
        stepStorys[animeNum].SetActive(false);
        stepStorys[setStep].SetActive(true);
        animeNum = setStep;
        checkStemName();
    }
    

    void checkStemName()
    {
        switch (animeNum)
        {
            case 0:
                step = StepName.InGame;
                break;
            case 1:
            case 2:
            case 3:
                step = StepName.Ikaku;
                break;

            case 15:
                if (legend)
                {
                    step = StepName.Ending;
                }
                else
                {
                    step = StepName.Result;
                }
                break;
            // レジェンドの場合
            case 20:
                step = StepName.Result;
                break;
            default:
                step = StepName.Ending;
                break;
        }
    }
    public void setRestart()
    {
        legend = false;
        setStepStory(0);
    }


 }
