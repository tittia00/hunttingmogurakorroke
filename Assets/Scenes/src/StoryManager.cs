﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoryManager : MonoBehaviour {

    //int SUKI_MAX = 500;
    //int SukiPoint = 0;

    public PointUpGauge ikakuGauge;
    public StepManager stepManager;

    public Text TextSukiNum;
    int WankoSuki = 500;

    public Text TextIkakuCount;
    int ikakuCount = 0;
    int ikakuMax = 10;

    public GameObject ikakuAttackView;
    public Text TextIkakuAttackPoint;

    public Text TextMain;
    public Text TextResultText;

    public GameObject infoView;
    bool startInfo = true;

    public GameObject gameOverView;
    bool gameOver = false;

    public GameObject textObjects;
    public GameObject restartObject;

    // Use this for initialization
    void Start () {
        setRestart();
	}
	
	// Update is called once per frame
	void Update ()
    {
        TextSukiNum.text = "" + WankoSuki;
        TextIkakuCount.text = "" + (ikakuMax-ikakuCount);

    }


    public IEnumerator autoEnding()
    {
        if (ikakuCount <= 6)
        {
            stepManager.legend = true;
        }
        while (stepManager.checkStep(StepManager.StepName.Ending))
        {
            yield return new WaitForSeconds(2);
            stepManager.setNextStory();
        }
        yield return new WaitForSeconds(2);

        if (ikakuCount <= 6)
        {
            TextResultText.text = "えいゆうど：レジェンドもぐコロ";
            stepManager.legend = true;
        }
        else if (ikakuCount <= 8)
        {
            TextResultText.text = "えいゆうど：ゆうしゃもぐコロ";
        }
        else if (ikakuCount <= 10)
        {
            TextResultText.text = "えいゆうど：ふつうもぐコロ";
        }
        restartObject.SetActive(true);
    }

    public void TapScreen()
    {
        //Debug.Log(ikakuGauge.getPoint());

        if (startInfo)
        {
            infoView.SetActive(false);
            startInfo = false;

            ikakuGauge.SetActiveGauge(true);
            return;
        }
        if (gameOver)
        {
            return;
        }

        if (stepManager.checkStep(StepManager.StepName.InGame))
        {
            WankoSuki -= ikakuGauge.getPoint();
            ikakuCount++;

            // スキが0になったらエンディングへ
            if (WankoSuki <= 0)
            {
                setEnding();
                return;
            }
            else
            {
                if (ikakuGauge.getPoint() >= 80)
                {
                    stepManager.setStepStory(3);
                    ikakuAttackView.SetActive(true);
                    TextIkakuAttackPoint.text = "" + ikakuGauge.getPoint();
                    ikakuGauge.SetActiveGauge(false);
                }
                else if (ikakuGauge.getPoint() >= 50)
                {
                    stepManager.setStepStory(2);
                    ikakuAttackView.SetActive(true);
                    TextIkakuAttackPoint.text = "" + ikakuGauge.getPoint();
                    ikakuGauge.SetActiveGauge(false);
                }
                else
                {
                    stepManager.setStepStory(1);
                    ikakuAttackView.SetActive(true);
                    TextIkakuAttackPoint.text = "" + ikakuGauge.getPoint();
                    ikakuGauge.SetActiveGauge(false);
                }
                StartCoroutine(setIkakuMode());
            }
        }
        else if (stepManager.checkStep(StepManager.StepName.Ending))
        {
        }
        else if (stepManager.checkStep(StepManager.StepName.Result))
        {
        }
    }
    
    void setGameOver()
    {
        ikakuGauge.SetActiveGauge(false);
        TextMain.text = "";
        textObjects.SetActive(false);
        gameOverView.SetActive(true);
        gameOver = true;
    }

    public void setEnding()
    {
        ikakuGauge.SetActiveGauge(false);
        WankoSuki = 0;
        TextMain.text = "";
        stepManager.setStepStory(4);

        textObjects.SetActive(false);
        restartObject.SetActive(false);
        StartCoroutine(autoEnding());
    }
    public IEnumerator setIkakuMode()
    {
        yield return new WaitForSeconds(2);

        ikakuAttackView.SetActive(false);
        stepManager.setStepStory(0);
        ikakuGauge.SetActiveGauge(true);

        // ゲームオーバー判定
        if ((ikakuMax - ikakuCount) <= 0)
        {
            setGameOver();
        }
    }

    public void setRestart()
    {
        TextMain.text = "がめんをタップしてスキをつくれ！！";
        TextResultText.text = "";
        WankoSuki = 500;
        ikakuCount = 0;
        stepManager.setRestart();


        ikakuAttackView.SetActive(false);
        ikakuGauge.SetActiveGauge(false);
        restartObject.SetActive(true);
        textObjects.SetActive(true);


        gameOverView.SetActive(false);
        gameOver = false;

        // infoViewの再表示
        infoView.SetActive(true);
        startInfo = true;
    }
}
