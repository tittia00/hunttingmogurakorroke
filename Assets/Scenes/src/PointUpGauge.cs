﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointUpGauge : MonoBehaviour {


    Slider _inputGauge;
    int gageAddCountParam = 1;
    bool ActibeGauge = false;

    // Use this for initialization
    void Start()
    {
        if (_inputGauge == null) { 
            _inputGauge = this.transform.gameObject.GetComponent<Slider>();
        }
    }

    public void SetActiveGauge(bool setActiveGauge)
    {
        //Debug.Log(setActiveGauge);
        ActibeGauge = setActiveGauge;

        if(_inputGauge == null)
        {
            _inputGauge = this.transform.gameObject.GetComponent<Slider>();
        }
        if (ActibeGauge)
        {
            _inputGauge.gameObject.SetActive(true);
        }
        else
        {
            _inputGauge.gameObject.SetActive(false);
        }
    }
	
	// Update is called once per frame
	void Update () {

        if (ActibeGauge)
        {
            if (_inputGauge.value >= 100)
            {
                gageAddCountParam = -5;
            }
            else if (_inputGauge.value <= 0)
            {

                gageAddCountParam = +5;
            }

            _inputGauge.value += gageAddCountParam;
        }
    }

    public int getPoint()
    {
        return (int)_inputGauge.value;
    }
}
